import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ICucmuser } from 'src/app/modeles/cucmuser';
import { AuthService } from 'src/app/services/auth.service';
import { CucmuserService } from 'src/app/services/cucmuser.service';

@Component({
    selector: 'app-cucmuser',
    templateUrl: './cucmuser.component.html',
    styleUrls: ['./cucmuser.component.css']
})
export class CucmuserComponent implements OnInit {

    public errorMessage;
    public index = 0;

    

    public cucmusers : ICucmuser[] = [];

    constructor(private _cucmuserService: CucmuserService,
        private authSerice: AuthService,
        private router: Router) { }

    ngOnInit(): void {
        if(this.authSerice.isLoggedIn()){
            this.getCucmusers();
        }else{
            alert("you must Login first");
            this.router.navigateByUrl("/login");
        }
        
    }

    getCucmusers(){
        this._cucmuserService.getCucmusers().subscribe(data => this.cucmusers = data,
            error => this.errorMessage = error);
    }

    deleteCucmuser(id: Number){
        console.log("delete cucmuser with id : ");
        console.log(id);
        if(window.confirm('Are sure you want to delete this item ?')){
            this._cucmuserService.deleteCucmuser(id).subscribe(
                response => {
                    alert(response.message);
                    this.getCucmusers();
                },
                error => {
                    alert(error.error.message);
                    this.getCucmusers();
                });
        }  
    }
    updateCucmuser(cucmuserId: Number){
        console.log("update user : " + cucmuserId);
        
        this.router.navigate(['cucmusers/edit', cucmuserId]);
    }
    hasPermission(permission): Boolean{
        return this.authSerice.hasPermission(permission);
    }

}