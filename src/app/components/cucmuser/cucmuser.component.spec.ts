import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CucmuserComponent } from './cucmuser.component';

describe('CucmuserComponent', () => {
  let component: CucmuserComponent;
  let fixture: ComponentFixture<CucmuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CucmuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CucmuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
