import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IUser } from 'src/app/modeles/user';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public errorMessage;
  public index = 0;
  public users: IUser[] = [];
  constructor(private _userService: UserService,
    private authSerice: AuthService,
    private router: Router) { }

  ngOnInit(): void {
    if(this.authSerice.isLoggedIn()){
      this.getUsers()  
    }else{
      alert("you must Login first");
      this.router.navigateByUrl("/login");
    }
  }
  getUsers() {
    this._userService.getUsers().subscribe(data => this.users = data,
      error => this.errorMessage = error);
  }


  deleteUser(username: String){
    console.log("delete user with username : ");
    console.log(username);
    if(window.confirm('Are sure you want to delete this user ?')){
        this._userService.deleteUser(username).subscribe(
            response => {
                alert(response.message);
                this.getUsers();
            },
            error => alert(error.error.message));
    }  
  }
  updateUser(username: String){
    console.log("update user : " + username);
        
    this.router.navigate(['/register', username]);
  }

  activateUserAcc(username: String){
    this._userService.activateUserAcc(username).subscribe(
      response => {
          alert(response.message);
          this.getUsers();
      },
      error => alert(error.error.message));
  }


  hasPermission(permission): Boolean{
    return this.authSerice.hasPermission(permission);
}
}
