import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LoginRequest } from 'src/app/modeles/LoginRequest';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

 
  loginForm: FormGroup;
  loginRequest: LoginRequest;

  isSending: Boolean = false;
  badLoginMessage: String;
  hidePassword: Boolean = true;

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

    this.password.valueChanges.subscribe(value => {
      if(value !== ""){
        this.badLoginMessage = undefined;
      }
      
    })
  }

  onSubmit() {
    this.badLoginMessage = undefined;
    this.isSending = true;
    this.loginRequest = {
      username: this.username.value,
      password: this.password.value
    }

    this.authService.login(this.loginRequest).subscribe(response => {
      console.log(response);

      this.authService.setUsername(response.username);
      this.authService.setPermissions(response.permissions);
      this.authService.setToken(response.token);

      this.router.navigateByUrl('/home');
    }, error=>{
      this.badLoginMessage = error.error.message;
      console.log("bad login");
      console.log(error);
      this.isSending = false;
      this.password.setValue("");
    });
  }
  
  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

}
