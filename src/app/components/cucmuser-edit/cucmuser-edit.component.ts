import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ICucmuser } from 'src/app/modeles/cucmuser';
import { CucmuserService } from 'src/app/services/cucmuser.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cucmuser-edit',
  templateUrl: './cucmuser-edit.component.html',
  styleUrls: ['./cucmuser-edit.component.css']
})
export class CucmuserEditComponent implements OnInit {

  cucmuserForm: FormGroup;

  cucmuser: ICucmuser;
  // = {
  //   id: "",
  //   name: "",
  //   password: "",
  //   local: ""
  // };
  
  isSending: boolean;

  constructor(private fb: FormBuilder,
    private cucmuserService: CucmuserService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.isSending = false;
    this.cucmuserForm = this.fb.group({
      name: ['', Validators.required],
      password: ['', Validators.required],
      local: ['', Validators.required]
    });

    let id = this.route.snapshot.params['id'];
    if (id !== undefined) {
      console.log(id);
      this.cucmuserService.getCucmuserById(id)
        .then(data => {
          console.log(data);
          this.cucmuser = data;
          let { name, password, local } = this.cucmuser;
          this.cucmuserForm.setValue({ name, password, local});
        })
        .catch(error => alert(error));
    }

  }

  onSubmit() {
    if (this.cucmuserForm.valid) {
      this.isSending = true;

      this.cucmuser = {
        cucmUserId: this.cucmuser === undefined? null : this.cucmuser.cucmUserId,
        name: this.name.value,
        password: this.password.value,
        local: this.local.value
      };

      this.cucmuserService.createUpdateCucmuser(this.cucmuser).subscribe(response => {
        this.router.navigateByUrl('/cucmusers');
      }, err => {
        if (err && err.hasOwnProperty('error') && err.error.errors.length > 0) {
          alert(err.error.errors.join('\n'));
          this.isSending = false;
        }
      });

    } else {
      alert('The value you intered are invalid!');
    }
  }

  get name() {
    return this.cucmuserForm.get('name');
  }
  get password() {
    return this.cucmuserForm.get('password');
  }
  get local() {
    return this.cucmuserForm.get('local');
  }

}
