import { Component, OnInit } from '@angular/core';
import { UserRole } from 'src/app/modeles/UserRole';
import { RegisterRequest } from 'src/app/modeles/RegisterRequest';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Route } from '@angular/compiler/src/core';
import { UserService } from 'src/app/services/user.service';
import { IUser } from 'src/app/modeles/user';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: IUser;
  urlUsername: String;
  roles: UserRole[] = [UserRole.USER, UserRole.MANAGER];

  registerForm: FormGroup;
  registerRequest: RegisterRequest;

  isSending: Boolean = false;
  errorMessage: String;

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      username: ['',  Validators.required],
      password: ['', Validators.required],
      password2: ['', Validators.required],
      role: ['', Validators.required],
    });

    this.urlUsername = this.route.snapshot.params['username'];
    if (this.urlUsername !== undefined) {
      console.log(this.urlUsername);
      this.userService.getUserByUsername(this.urlUsername)
        .then(data => {
          console.log(data);
          this.user = data;
          let { username, role } = this.user;
          this.registerForm.setValue({ username, password: '', password2: '', role });
        })
        .catch(error => console.log(error));
    }
  }

  onSubmit() {
    this.errorMessage = undefined;
    this.isSending = true;
    
    this.registerRequest= {
      username: this.username.value,
      password: this.password.value,
      role: this.role.value
    }

    if(this.user !== undefined){
      console.log("updating user");
      this.authService.updateUser(this.urlUsername, this.registerRequest).subscribe(response => {
        console.log(response);
        alert("user has been changed")
        this.router.navigateByUrl('/users');
      }, error=>{
        alert(error.error.message);
        this.username.setValue(this.urlUsername);
        console.log(error);
        this.isSending = false;
        this.password.setValue("");
        this.password2.setValue("");
      });
    }else{
      this.authService.register(this.registerRequest).subscribe(response => {
        console.log(response);
        if(!this.authService.isLoggedIn()){
          this.router.navigateByUrl('/login');
        }else{
          this.router.navigateByUrl('/users');
        }
        
      }, error=>{
        this.errorMessage = "error registration";
        // alert(error.error);
        console.log(error.error);
        this.isSending = false;
        this.password.setValue("");
        this.password2.setValue("");
      });
    }
  }
  
  get username() {
    return this.registerForm.get('username');
  }

  get password() {
    return this.registerForm.get('password');
  }
  get password2() {
    return this.registerForm.get('password2');
  }

  get role(){
    return this.registerForm.get('role');
  }

}
