import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { ICucmuser } from 'src/app/modeles/cucmuser';
import { Observable } from 'rxjs';
import { throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CucmuserService {
  private _cucmusersUrl = "http://localhost:8080/cucmusers";
  public headers: HttpHeaders;

  constructor(private http : HttpClient,
    private _authService: AuthService) { }


  getCucmusers(): Observable<ICucmuser[]> {
    this.headers = this._authService.getTokenHeader();
    // let headers = this._authService.getTokenHeader();
    return this.http.get<ICucmuser[]>(this._cucmusersUrl, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }

  getCucmuserById(id): Promise<ICucmuser>{
    this.headers = this._authService.getTokenHeader();
    // let headers = this._authService.getTokenHeader();
    return this.http.get<ICucmuser>(this._cucmusersUrl + "/" + id, 
      {headers: this.headers}).toPromise();
  }

  deleteCucmuser(id: Number): Observable<any>{
    console.log("delete methode");
    this.headers = this._authService.getTokenHeader();
    // this.headers.set('responseType', 'text');
    return this.http.delete<String>(this._cucmusersUrl + "/" + id, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }



  createUpdateCucmuser(cucmuser: ICucmuser){
    this.headers = this._authService.getTokenHeader();
    return this.http.post<any>(this._cucmusersUrl, cucmuser, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }

  errorHandler(error : HttpErrorResponse){
    return throwError(error || "server error !!");
  }
}
