import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, } from '@angular/common/http';
import { IUser } from 'src/app/modeles/user';
import { Observable } from 'rxjs';
import { throwError} from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';


// export const HTTP_OPTIONS = {
//   headers: new HttpHeaders({
//     'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ6YWthcmlhIiwiZXhwIjoxNjAwNTg5OTQwLCJpYXQiOjE2MDA1NzE5NDB9.HFQLy-yi7kO7YgPYsyh2OvyLVc7iqlHSWKnmReF29g8eHhtiAI5PnLx3BCqhepu6QI1wE_ERbrX_IOUuGsIpOw',
//     'Content-Type':  'application/json',
//     'Access-Control-Allow-Credentials' : 'false',
//     'Access-Control-Allow-Origin': '*',
//     'Access-Control-Allow-Methods': 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
//     'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
  
//   })
// };

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _usersUrl = "http://localhost:8080/users";
  public headers: HttpHeaders;
  
  constructor(private http : HttpClient,
    private _authService: AuthService) { }


 
  

  getUsers(): Observable<IUser[]> {
    // let token = "";
    // let authHeader = {'Authorization': token};
    // let headers: HttpHeaders = new HttpHeaders(authHeader);
    // headers.set("Authorization", "Bearw");
    console.log(this._authService.isLoggedIn());
    let headers = this._authService.getTokenHeader();
    
    return this.http.get<IUser[]>(this._usersUrl, {headers: headers}).pipe(catchError(this.errorHandler));
  }

  getUserByUsername(username: String): Promise<IUser>{
    let headers = this._authService.getTokenHeader();
    
    return this.http.get<IUser>(this._usersUrl + "/" + username, {headers: headers}).toPromise();
  }

  deleteUser(username: String): Observable<any>{
    console.log("delete methode");
    this.headers = this._authService.getTokenHeader();
    return this.http.delete<any>(this._usersUrl + "/" + username, {headers: this.headers}).pipe(catchError(this.errorHandler));
  }

  createUpdateUser(user: IUser){
    return this.http.post<any>(this._usersUrl, user).pipe(catchError(this.errorHandler));
  }

  activateUserAcc(username: String): Observable<any>{
    this.headers = this._authService.getTokenHeader();
    return this.http.put<any>(this._usersUrl + "/activataccount" + "/" + username, null,  {headers: this.headers}).pipe(catchError(this.errorHandler));
  }

  errorHandler(error : HttpErrorResponse){
    return throwError(error || "server error !!");
  }
}
